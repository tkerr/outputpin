/******************************************************************************
 * OutputPinExample.ino
 * Copyright (c) 2021 Tom Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * OutputPin example sketch.
 */
 

/******************************************************************************
 * Lint options.
 ******************************************************************************/
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "OutputPin.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define BOARD_LED_PIN     (13)  //!< The Arduino LED pin
#define BOARD_LED_ON       (1)  //!< Logic 1 turns the LED on
#define LED_INTERVAL_MS (1000)  //!< LED state change interval in ms

/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
OutputPin arduinoLedPin;


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static uint32_t last = 0;


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup() 
{
    arduinoLedPin.init(BOARD_LED_PIN, BOARD_LED_ON);
    
    for (int i = 0; i < 20; i++)
    {
        arduinoLedPin.turnOn();
        delay(100);
        arduinoLedPin.turnOff();
        delay(100);
    }
    last = millis();
}


/**************************************
 * loop()
 **************************************/ 
void loop() 
{
    uint32_t now = millis();
    if ((now - last) >= LED_INTERVAL_MS)
    {
        last = now;
        arduinoLedPin.toggle();
    }
}

// End of file.
