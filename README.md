# OutputPin #
Arduino "Syntactic Sugar" class to encapsulate a single hardware output pin.

Provides convenient on/off/toggle functions without having to preserve the
on/off digital values everywhere in the code.

### Documentation ###
Documentation is contained in the OutputPin.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License   
https://opensource.org/licenses/MIT
