/******************************************************************************
 * OutputPin.cpp
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Arduino "Syntactic Sugar" class to encapsulate a single hardware 
 * digital output pin.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "OutputPin.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
/**************************************
 * OutputPin::OutputPin()
 **************************************/ 
OutputPin::OutputPin() :
    m_pin(-1),
    m_on_val(1),
    m_pin_val(0)
{
    // Nothing to do.
    // Must still initialize using init().
}


/**************************************
 * OutputPin::OutputPin()
 **************************************/ 
OutputPin::OutputPin(int pin, int on_val)
{
    init(pin, on_val);
}


/**************************************
 * OutputPin::init()
 **************************************/ 
void OutputPin::init(int pin, int on_val)
{
    m_pin = pin;
    m_on_val = on_val;
    m_pin_val = (on_val == 0)? 1 : 0;  // Init state to OFF
    if (m_pin >= 0)
    {
        pinMode(m_pin, OUTPUT);
        digitalWrite(m_pin, m_pin_val);
    }
}


/**************************************
 * OutputPin::turnOff()
 **************************************/ 
void OutputPin::turnOff()
{
    if (m_pin >= 0)
    {
        m_pin_val = 1 - m_on_val;
        digitalWrite(m_pin, m_pin_val);
    }
}


/**************************************
 * OutputPin::turnOn()
 **************************************/ 
void OutputPin::turnOn()
{
    if (m_pin >= 0)
    {
        m_pin_val = m_on_val;
        digitalWrite(m_pin, m_pin_val);
    }
}


/**************************************
 * OutputPin::toggle()
 **************************************/ 
void OutputPin::toggle()
{
    if (m_pin >= 0)
    {
        m_pin_val = 1 - m_pin_val;
        digitalWrite(m_pin, m_pin_val);
    }
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

 
// End of file.
