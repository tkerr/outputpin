/******************************************************************************
 * OutputPin.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino "Syntactic Sugar" class to encapsulate a single hardware output pin.
 */

#ifndef _OUTPUT_PIN_H
#define _OUTPUT_PIN_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @brief Arduino "Syntactic Sugar" class to encapsulate a single hardware 
 * digital output pin.
 *
 * Provides convenient on/off/toggle functions without having to know the
 * on/off digital values.
 */
class OutputPin
{
public:

    /**
     * @brief Default constructor.
     *
     * Must initialize using init() method.
     */
    OutputPin();
    
    /**
     * @brief Construct and initialize using the pin number and pin on value.
     *
     * @param pin The Arduino pin number.  Ignored if negative.
     * 
     * @param on_val The pin 'on' logic value (0/1). Default is 1.
     */
    OutputPin(int pin, int on_val=1);
    
    /**
     * @brief Initialize the OutputPin object.
     *
     * @param pin The Arduino pin number.
     * 
     * @param on_val The output pin 'on' logic value (0/1).
     */
    void init(int pin, int on_val);
    
    /**
     * @brief Return the output pin state.
     * @return True = pin is on, false = pin is off.
     */
    inline bool isOn() {return m_pin_val == m_on_val;}
    
    /**
     * @brief Return the actual value of the output pin (0 = low, 1 = high).
     */
    inline int pinVal() {return m_pin_val;}
    
    /**
     * @brief Turn the output pin off.
     */
    void turnOff(void);
    
    /**
     * @brief Turn the output pin on.
     */
    void turnOn(void);
    
    /**
     * @brief Toggle the output pin state.
     */
    void toggle(void);
    
private:

    int m_pin;      //!< output pin number
    int m_on_val;   //!< output pin 'on' logic value (0/1)
    int m_pin_val;  //!< actual value written to output pin (0/1)
};


#endif // _OUTPUT_PIN_H
